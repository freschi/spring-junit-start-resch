package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import at.spenger.junit.ApplicationTestAbstract;

public class PersonTest extends ApplicationTestAbstract {

	private Person p;
	private static final String P_DATE = "1950.09.27";
	
	@Before
	public void setUp() throws Exception {
		p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
	}

	@Test
	public void testGetBirthdayString() {
		String s = p.getBirthdayString();
		assertEquals(P_DATE, s);
	}

}
